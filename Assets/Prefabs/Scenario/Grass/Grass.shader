// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "New AmplifyShader"
{
	Properties
	{
		_grass_01_DefaultMaterial_BaseColor("grass_01_DefaultMaterial_BaseColor", 2D) = "white" {}
		_Amplitude("Amplitude", Range( 0 , 1)) = 0
		_RandomTintA("Random Tint A", Color) = (0,0,0,0)
		_RandomTintB("Random Tint B", Color) = (0,0,0,0)
		_Texture0("Texture 0", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _RandomTintA;
		uniform sampler2D _grass_01_DefaultMaterial_BaseColor;
		uniform float4 _grass_01_DefaultMaterial_BaseColor_ST;
		uniform float4 _RandomTintB;
		uniform sampler2D _Texture0;
		uniform float _Amplitude;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float mulTime17 = _Time.y * 15;
			float4 appendResult16 = (float4(sin( ( mulTime17 - v.color.r ) ) , 0 , 0 , 0));
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float mulTime33 = _Time.y * 1;
			float4 appendResult30 = (float4(sin( ( ( ase_worldPos.x + mulTime33 ) - v.color.r ) ) , 0 , 0 , 0));
			v.vertex.xyz += ( ( ( ( appendResult16 * 0.003 ) * v.color.r ) * _Amplitude ) + ( ( appendResult30 * 0.05 ) * v.color.r ) ).xyz;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_grass_01_DefaultMaterial_BaseColor = i.uv_texcoord * _grass_01_DefaultMaterial_BaseColor_ST.xy + _grass_01_DefaultMaterial_BaseColor_ST.zw;
			float4 tex2DNode6 = tex2D( _grass_01_DefaultMaterial_BaseColor, uv_grass_01_DefaultMaterial_BaseColor );
			float4 transform41 = mul(unity_WorldToObject,float4( 0,0,0,1 ));
			float2 appendResult42 = (float2(transform41.x , transform41.z));
			float4 lerpResult46 = lerp( ( _RandomTintA * tex2DNode6 ) , ( tex2DNode6 * _RandomTintB ) , ( tex2D( _Texture0, appendResult42 ).r * 1.25 ));
			o.Albedo = lerpResult46.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1913;29;1906;1004;1785.693;1410.183;1;True;True
Node;AmplifyShaderEditor.VertexColorNode;23;-1759.787,-189.5196;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;17;-1754.492,-265.8143;Float;False;1;0;FLOAT;15;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;38;-1768.596,7.216797;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleTimeNode;33;-1707.865,209.3293;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;25;-1573.787,-208.5196;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;27;-1590.768,324.3806;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;37;-1435.596,128.2168;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;28;-1237.007,244.3768;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;20;-1437.492,-208.8143;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-1304.406,-49.37705;Float;False;Constant;_Float0;Float 0;2;0;Create;True;0;0.003;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldToObjectTransfNode;41;-1429.954,-907.6093;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinOpNode;29;-1100.71,244.0821;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;16;-1301.492,-202.8143;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;30;-964.7101,250.0821;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TexturePropertyNode;43;-1318.954,-1115.609;Float;True;Property;_Texture0;Texture 0;4;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;False;white;Auto;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.DynamicAppendNode;42;-1223.954,-869.6093;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-1087.205,-147.6769;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;32;-967.624,404.5195;Float;False;Constant;_Float1;Float 1;2;0;Create;True;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;15;-1098.491,-12.81431;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-750.4242,305.2196;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ColorNode;47;-815.0959,-677.7832;Float;False;Property;_RandomTintA;Random Tint A;2;0;Create;True;0;0,0,0,0;0.6573619,0.4424741,0.9705882,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;44;-1036.954,-994.6093;Float;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;6;-881.8882,-481.649;Float;True;Property;_grass_01_DefaultMaterial_BaseColor;grass_01_DefaultMaterial_BaseColor;0;0;Create;True;0;06435536fb9cb6f4ea6da7d62e819ce2;06435536fb9cb6f4ea6da7d62e819ce2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-778.9501,-76.45737;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexColorNode;34;-747.9572,409.234;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;40;-783.5964,-291.7832;Float;False;Property;_RandomTintB;Random Tint B;3;0;Create;True;0;0,0,0,0;0.3078504,0.5272924,0.5367647,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;21;-887.4894,63.1857;Float;False;Property;_Amplitude;Amplitude;1;0;Create;True;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-551.5134,323.8038;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-616.8054,-1063.653;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;1.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;39;-494.5964,-347.7832;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-502.0959,-523.7832;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-598.4896,-69.8143;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;-422.6125,75.09106;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;46;-247.454,-539.0092;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-143,-294;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;New AmplifyShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;25;0;17;0
WireConnection;25;1;23;1
WireConnection;37;0;38;1
WireConnection;37;1;33;0
WireConnection;28;0;37;0
WireConnection;28;1;27;1
WireConnection;20;0;25;0
WireConnection;29;0;28;0
WireConnection;16;0;20;0
WireConnection;30;0;29;0
WireConnection;42;0;41;1
WireConnection;42;1;41;3
WireConnection;13;0;16;0
WireConnection;13;1;14;0
WireConnection;31;0;30;0
WireConnection;31;1;32;0
WireConnection;44;0;43;0
WireConnection;44;1;42;0
WireConnection;8;0;13;0
WireConnection;8;1;15;1
WireConnection;35;0;31;0
WireConnection;35;1;34;1
WireConnection;45;0;44;1
WireConnection;39;0;6;0
WireConnection;39;1;40;0
WireConnection;48;0;47;0
WireConnection;48;1;6;0
WireConnection;22;0;8;0
WireConnection;22;1;21;0
WireConnection;26;0;22;0
WireConnection;26;1;35;0
WireConnection;46;0;48;0
WireConnection;46;1;39;0
WireConnection;46;2;45;0
WireConnection;0;0;46;0
WireConnection;0;11;26;0
ASEEND*/
//CHKSM=918D9C8353FBEAEC945787A6DE168AC9B391E501