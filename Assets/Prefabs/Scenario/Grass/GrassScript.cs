﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassScript : MonoBehaviour {

    public float ENGAGE_SPEED = 0.3f;
    public float DECAY_SPEED = 0.1f;
    public float STRENGTH = 1;

    private Material myMaterial;
    private float targetAmplitude = 0;
    private float currentAmplitude = 0;
    private bool active = false;
    private int state = 0; // 0=waiting   1=increasing   2=decreasing

    private AudioSource AS;

    private void Awake() {
        AS = GetComponent<AudioSource>();
    }

    void Start () {
        myMaterial = GetComponent<Renderer>().material;
	}

	void FixedUpdate () {
        switch (state) {
            case 0://Grass is waiting
                break;
            case 1://Movement is increasing
                currentAmplitude = Mathf.Lerp(currentAmplitude, targetAmplitude, ENGAGE_SPEED);
                if (currentAmplitude > targetAmplitude-0.05f) {
                    targetAmplitude = 0;
                    state = 2;
                }
                myMaterial.SetFloat("_Amplitude", currentAmplitude);
                break;
            case 2://Movement is decreasing
                currentAmplitude = Mathf.Lerp(currentAmplitude, targetAmplitude, DECAY_SPEED);
                if (currentAmplitude < 0.01f) {
                    currentAmplitude = 0;
                    state = 0;
                }
                myMaterial.SetFloat("_Amplitude", currentAmplitude);
                break;
            default:
                break;
        }
	}

    public void Activate() {
        targetAmplitude = STRENGTH;
        state = 1;

        AS.pitch = Random.Range(0.9f, 1.25f);
        AS.Play();
    }
}
