// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Fuego"
{
	Properties
	{
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_fire_1("fire_1", 2D) = "white" {}
		_WarpIntensity("Warp Intensity", Range( 0 , 1)) = 0.5399377
		_fire_mask_2("fire_mask_2", 2D) = "white" {}
		_EmissiveIntensity("Emissive Intensity", Range( 1 , 5)) = 1.69
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _fire_mask_2;
		uniform float _WarpIntensity;
		uniform sampler2D _TextureSample0;
		uniform sampler2D _fire_1;
		uniform float _EmissiveIntensity;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TexCoord9 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float4 transform30 = mul(unity_WorldToObject,float4( 0,0,0,1 ));
			float2 uv_TexCoord6 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 temp_output_32_0 = ( transform30.x + uv_TexCoord6 );
			float2 panner3 = ( temp_output_32_0 + 1 * _Time.y * float2( 0,-1.2 ));
			float4 tex2DNode5 = tex2D( _TextureSample0, panner3 );
			float2 panner4 = ( temp_output_32_0 + 1 * _Time.y * float2( 0,-1.5 ));
			float4 tex2DNode1 = tex2D( _fire_1, panner4 );
			float2 appendResult18 = (float2(uv_TexCoord9.x , ( ( ( _WarpIntensity * ( tex2DNode5.r * tex2DNode1.g ) ) * -1 ) + uv_TexCoord9.y )));
			float4 tex2DNode19 = tex2D( _fire_mask_2, appendResult18 );
			float4 lerpResult23 = lerp( float4(1,0,0,0) , float4(1,0.6827587,0,0) , tex2DNode19.r);
			o.Emission = ( lerpResult23 * _EmissiveIntensity ).rgb;
			o.Alpha = tex2DNode19.r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1913;29;1906;1004;2537.872;773.5181;1;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;-1985.74,-130.5725;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldToObjectTransfNode;30;-1981.872,-379.5181;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;32;-1620.872,-282.5181;Float;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;3;-1567.74,-194.5726;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1.2;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;4;-1631.74,-18.5725;Float;False;3;0;FLOAT2;1,1;False;2;FLOAT2;0,-1.5;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;5;-1359.74,-226.5726;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;4aaa14a6f50a8ef409555d1879f69024;4aaa14a6f50a8ef409555d1879f69024;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-1359.74,-18.5725;Float;True;Property;_fire_1;fire_1;1;0;Create;True;0;4aaa14a6f50a8ef409555d1879f69024;4aaa14a6f50a8ef409555d1879f69024;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;16;-1044.601,-418.5726;Float;False;Property;_WarpIntensity;Warp Intensity;2;0;Create;True;0;0.5399377;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1016.232,-319.3082;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-768.275,-397.1476;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-799.7394,-2.572502;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-557.5967,-466.8076;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-528.7393,-175.5923;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;18;-378.5232,22.26467;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;20;-199.0049,-265.2092;Float;False;Constant;_Color0;Color 0;5;0;Create;True;0;1,0.6827587,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;24;-192.1389,-438.0111;Float;False;Constant;_Color1;Color 1;5;0;Create;True;0;1,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;19;-214.765,74.21831;Float;True;Property;_fire_mask_2;fire_mask_2;3;0;Create;True;0;b03dd6d99362fd942a25c6b2e17f5de6;c31f60316520d6b4f9df74dd51481725;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;23;104.8611,-183.0111;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;22;97.11386,-380.2433;Float;False;Property;_EmissiveIntensity;Emissive Intensity;4;0;Create;True;0;1.69;2.694118;1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;29;186.415,304.2056;Float;True;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;28;13.11787,307.7476;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-190.4229,291.5508;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;395.744,-205.642;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;2;-1007.74,-98.57251;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;592.1953,-205.1099;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Fuego;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;Back;0;0;False;0;0;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;32;0;30;1
WireConnection;32;1;6;0
WireConnection;3;0;32;0
WireConnection;4;0;32;0
WireConnection;5;1;3;0
WireConnection;1;1;4;0
WireConnection;25;0;5;1
WireConnection;25;1;1;2
WireConnection;15;0;16;0
WireConnection;15;1;25;0
WireConnection;26;0;15;0
WireConnection;10;0;26;0
WireConnection;10;1;9;2
WireConnection;18;0;9;1
WireConnection;18;1;10;0
WireConnection;19;1;18;0
WireConnection;23;0;24;0
WireConnection;23;1;20;0
WireConnection;23;2;19;1
WireConnection;29;0;28;0
WireConnection;28;0;27;0
WireConnection;27;0;19;1
WireConnection;21;0;23;0
WireConnection;21;1;22;0
WireConnection;2;0;5;1
WireConnection;2;1;1;2
WireConnection;0;2;21;0
WireConnection;0;9;19;1
ASEEND*/
//CHKSM=47858B086DF5CC15C9A1EC489EAA22FD7B38EBD8