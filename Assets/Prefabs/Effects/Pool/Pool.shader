// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Pool"
{
	Properties
	{
		_DepthBias("Depth Bias", Range( 0 , 100)) = 10.6
		_Float3("Float 3", Range( 0 , 100)) = 10.6
		_ShallowColor("Shallow Color", Color) = (0,0,0,0)
		_DeepColor("Deep Color", Color) = (0,0,0,0)
		_Texture0("Texture 0", 2D) = "white" {}
		[HideInInspector]_Float4("Float 4", Range( 0 , 1)) = 0
		_EdgeColor("Edge Color", Color) = (1,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGINCLUDE
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float4 screenPos;
			float2 uv_texcoord;
		};

		uniform float4 _EdgeColor;
		uniform float4 _ShallowColor;
		uniform float4 _DeepColor;
		uniform sampler2D _CameraDepthTexture;
		uniform float _DepthBias;
		uniform sampler2D _Texture0;
		uniform float _Float3;
		uniform float _Float4;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 uv_TexCoord4 = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float2 panner3 = ( uv_TexCoord4 + 2 * _Time.y * float2( 0.1,0.1 ));
			float2 uv_TexCoord17 = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float2 panner18 = ( uv_TexCoord17 + 2 * _Time.y * float2( -0.1,-0.1 ));
			float2 uv_TexCoord23 = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float2 panner25 = ( uv_TexCoord23 + 2 * _Time.y * float2( 0.1,-0.1 ));
			float temp_output_26_0 = ( ( tex2Dlod( _Texture0, float4( panner3, 0, 0) ).r * tex2Dlod( _Texture0, float4( panner18, 0, 0) ).g ) * tex2Dlod( _Texture0, float4( panner25, 0, 0) ).b );
			float3 appendResult2 = (float3(0 , temp_output_26_0 , 0));
			v.vertex.xyz += ( appendResult2 * 4.0 );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth6 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth6 = abs( ( screenDepth6 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( 1 ) );
			float2 uv_TexCoord4 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 panner3 = ( uv_TexCoord4 + 2 * _Time.y * float2( 0.1,0.1 ));
			float2 uv_TexCoord17 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 panner18 = ( uv_TexCoord17 + 2 * _Time.y * float2( -0.1,-0.1 ));
			float2 uv_TexCoord23 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 panner25 = ( uv_TexCoord23 + 2 * _Time.y * float2( 0.1,-0.1 ));
			float temp_output_26_0 = ( ( tex2D( _Texture0, panner3 ).r * tex2D( _Texture0, panner18 ).g ) * tex2D( _Texture0, panner25 ).b );
			float clampResult28 = clamp( ( ( distanceDepth6 / _DepthBias ) + (0 + (temp_output_26_0 - 0) * (1 - 0) / (0.5 - 0)) ) , 0 , 1 );
			float4 lerpResult11 = lerp( _ShallowColor , _DeepColor , clampResult28);
			float screenDepth45 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth45 = abs( ( screenDepth45 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( 1 ) );
			float4 lerpResult51 = lerp( _EdgeColor , lerpResult11 , floor( ( ( distanceDepth45 / _Float3 ) + _Float4 ) ));
			o.Albedo = lerpResult51.rgb;
			o.Smoothness = 0.0;
			o.Alpha = 0.5;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1913;29;1906;1004;818.1847;974.7593;1;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;4;-1433,215;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;17;-1408.667,414.6667;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;23;-1401,619.6667;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;3;-1219,215;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.1,0.1;False;1;FLOAT;2;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;18;-1193.667,414.6667;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.1,-0.1;False;1;FLOAT;2;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;29;-1231.632,-64.64743;Float;True;Property;_Texture0;Texture 0;4;0;Create;True;0;None;275e3ab0ff482a1488ab55465aef9497;False;white;Auto;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;1;-1048,186;Float;True;Property;_noise;noise;2;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;19;-1023.667,385.6667;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;25;-1186,619.6667;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.1,-0.1;False;1;FLOAT;2;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-715,315;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;24;-1016,590.6667;Float;True;Property;_TextureSample1;Texture Sample 1;0;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;9;-555,54;Float;False;Property;_DepthBias;Depth Bias;0;0;Create;True;0;10.6;7.8;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.DepthFade;6;-477,-25;Float;False;True;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-441,414;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;10;-279,2;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;42;-113.1847,617.2407;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DepthFade;45;-583.1847,-623.0927;Float;False;True;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-661.1847,-544.0927;Float;False;Property;_Float3;Float 3;1;0;Create;True;0;10.6;32.50588;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-470.1847,-431.7593;Float;False;Property;_Float4;Float 4;5;1;[HideInInspector];Create;True;0;0;0.99;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;14;-90,86;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;44;-385.1847,-596.0927;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;12;-100,-214;Float;False;Property;_DeepColor;Deep Color;3;0;Create;True;0;0,0,0,0;0.1760768,0.1764706,0.1479239,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;13;-102,-381;Float;False;Property;_ShallowColor;Shallow Color;2;0;Create;True;0;0,0,0,0;0.4972736,0.6029412,0.4832396,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;47;-258.1847,-529.7593;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;28;24,-5;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;22;-25,496;Float;False;Constant;_WarpStrength;Warp Strength;5;0;Create;True;0;4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;53;219.8153,-734.7593;Float;False;Property;_EdgeColor;Edge Color;6;0;Create;True;0;1,0,0,0;0.7084775,0.7647059,0.7181721,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FloorOpNode;46;-142.1847,-520.7593;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;2;-180,391;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;11;185,-227;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-420,236;Float;False;Constant;_Float1;Float 1;4;0;Create;True;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;172,379;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;35;316.9186,-15.52289;Float;False;Constant;_Float2;Float 2;4;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;291.0184,117.9151;Float;False;Constant;_Float0;Float 0;4;0;Create;True;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;51;399.8153,-468.7593;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-226,217;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;474,-99;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Pool;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;Back;0;0;False;0;0;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;0;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;4;0
WireConnection;18;0;17;0
WireConnection;1;0;29;0
WireConnection;1;1;3;0
WireConnection;19;0;29;0
WireConnection;19;1;18;0
WireConnection;25;0;23;0
WireConnection;20;0;1;1
WireConnection;20;1;19;2
WireConnection;24;0;29;0
WireConnection;24;1;25;0
WireConnection;26;0;20;0
WireConnection;26;1;24;3
WireConnection;10;0;6;0
WireConnection;10;1;9;0
WireConnection;42;0;26;0
WireConnection;14;0;10;0
WireConnection;14;1;42;0
WireConnection;44;0;45;0
WireConnection;44;1;43;0
WireConnection;47;0;44;0
WireConnection;47;1;48;0
WireConnection;28;0;14;0
WireConnection;46;0;47;0
WireConnection;2;1;26;0
WireConnection;11;0;13;0
WireConnection;11;1;12;0
WireConnection;11;2;28;0
WireConnection;21;0;2;0
WireConnection;21;1;22;0
WireConnection;51;0;53;0
WireConnection;51;1;11;0
WireConnection;51;2;46;0
WireConnection;15;0;16;0
WireConnection;15;1;26;0
WireConnection;0;0;51;0
WireConnection;0;4;35;0
WireConnection;0;9;30;0
WireConnection;0;11;21;0
ASEEND*/
//CHKSM=5BE5CB169EAE42D5C7F907F51A798F7A365B95BA