﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flotador : MonoBehaviour {

    public float AMPLITUDE = 0.1f;
    public float SPEED = 1.0f;

    private Vector3 position;

	// Use this for initialization
	void Start () {
        position = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = position + new Vector3(0, AMPLITUDE * Mathf.Sin(Time.time * SPEED), 0);
	}
}
