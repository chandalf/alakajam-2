// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Slash"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.15
		_position("position", Range( -1 , 1)) = -1
		_Slash("Slash", 2D) = "white" {}
		_fade("fade", 2D) = "white" {}
		_Color("Color", Color) = (0,0.6275863,1,0)
		_Intensity("Intensity", Range( 0 , 10)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color;
		uniform float _Intensity;
		uniform sampler2D _Slash;
		uniform float4 _Slash_ST;
		uniform sampler2D _fade;
		uniform float _position;
		uniform float _Cutoff = 0.15;

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Emission = ( _Color * _Intensity ).rgb;
			o.Alpha = 1;
			float2 uv_Slash = i.uv_texcoord * _Slash_ST.xy + _Slash_ST.zw;
			float2 appendResult24 = (float2(_position , 0.0));
			float2 uv_TexCoord23 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 uv_TexCoord37 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			clip( ( ( ( tex2D( _Slash, uv_Slash ) * tex2D( _fade, ( appendResult24 + uv_TexCoord23 ) ) ) + 0.0 ) * ( 1.0 - ( distance( uv_TexCoord37.x , 0.5 ) * 1.1 ) ) ).r - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
234;92;1284;732;568.3936;703.3724;1.260382;True;True
Node;AmplifyShaderEditor.RangedFloatNode;26;-911.3652,133.7193;Float;False;Constant;_Float0;Float 0;4;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1039.023,55.20033;Float;False;Property;_position;position;1;0;Create;True;0;-1;-0.2;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;24;-722.5249,69.52853;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;23;-1000.579,219.3501;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;37;-615.3901,551.5814;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;27;-532.0156,169.1658;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;40;-545.2887,662.4577;Float;False;Constant;_Float1;Float 1;4;0;Create;True;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;21;-383.4705,141.3956;Float;True;Property;_fade;fade;3;0;Create;True;0;0bfe8fece4c6f2041b61b22c1c5e9769;0bfe8fece4c6f2041b61b22c1c5e9769;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;12;-386.0927,-61.1082;Float;True;Property;_Slash;Slash;2;0;Create;True;0;3607024cdd09f4d4fa928f2219c9dff1;3607024cdd09f4d4fa928f2219c9dff1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;45;-280.5527,402.0973;Float;False;Constant;_Float2;Float 2;4;0;Create;True;0;1.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;39;-366.2887,541.4577;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-120.5527,497.0973;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;32;-59.84837,296.3592;Float;False;Constant;_CutoutThreshold;Cutout Threshold;4;0;Create;True;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-3.046978,70.56034;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;41;-31.2887,605.4577;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;31;253.351,241.0888;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;30;-123.1187,-429.2778;Float;False;Property;_Color;Color;4;0;Create;True;0;0,0.6275863,1,0;0,0.6275863,1,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;47;-62.9805,-253.416;Float;False;Property;_Intensity;Intensity;5;0;Create;True;0;1;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;152.5447,-345.4238;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;451.2561,372.0386;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;549.2452,-159.6688;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Slash;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Custom;0.15;True;True;0;True;TransparentCutout;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;24;0;11;0
WireConnection;24;1;26;0
WireConnection;27;0;24;0
WireConnection;27;1;23;0
WireConnection;21;1;27;0
WireConnection;39;0;37;1
WireConnection;39;1;40;0
WireConnection;43;0;39;0
WireConnection;43;1;45;0
WireConnection;9;0;12;0
WireConnection;9;1;21;0
WireConnection;41;0;43;0
WireConnection;31;0;9;0
WireConnection;31;1;32;0
WireConnection;48;0;30;0
WireConnection;48;1;47;0
WireConnection;42;0;31;0
WireConnection;42;1;41;0
WireConnection;0;2;48;0
WireConnection;0;10;42;0
ASEEND*/
//CHKSM=36D90FB7FD0BBAC1E6E85AEDD28A71F8DF6338DA