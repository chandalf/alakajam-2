﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public enum ESpanwPointButtonID {X, Y, B}

    public ESpanwPointButtonID currentButtonID;
    public int currentPlayer;

    [SerializeField]
    private GameObject BButtonSelectorGO;
    [SerializeField]
    private GameObject YButtonSelectorGO;
    [SerializeField]
    private GameObject XButtonSelectorGO;

    private GameObject currentSelector;


    //SETUP SPAWN POINT
    public void SetupSpawnPoint(ESpanwPointButtonID buttonID, int player) {
        currentButtonID = buttonID;
        currentPlayer = player;

        switch (buttonID) {
            case ESpanwPointButtonID.B:
                currentSelector = Instantiate(BButtonSelectorGO, transform.position, Quaternion.identity);
                break;
            case ESpanwPointButtonID.X:
                currentSelector = Instantiate(XButtonSelectorGO, transform.position, Quaternion.identity);
                break;
            case ESpanwPointButtonID.Y:
                currentSelector = Instantiate(YButtonSelectorGO, transform.position, Quaternion.identity);
                break;
        }

        switch (player) {
            case 1:
                foreach(Renderer rend in currentSelector.GetComponentsInChildren<Renderer>()) {
                    rend.material.SetColor("_Color0", GameManager.instance.player1Color);
                }
                break;
            case 2:
                foreach (Renderer rend in currentSelector.GetComponentsInChildren<Renderer>()) {
                    rend.material.SetColor("_Color0", GameManager.instance.player2Color);
                }
                break;
        }
    }

    //SHOW UI
    public void ShowUI(bool value) {
        Destroy(currentSelector);
    }

    //ON DRAW GIZMOS SELECTED
    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 0.4f);
    }
}
