﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mask : MonoBehaviour {

    public static Mask instance;

    public float powerUpDuration = 5f;

    public Transform player1Position;
    public Transform player2Position;

    public Revelador player1Revelador;
    public Revelador player2Revelador;

    [HideInInspector]
    public ManaBall player1ManaBall;
    [HideInInspector]
    public ManaBall player2ManaBall;

    private float player1EndPowerUpTime;
    private float player2EndPowerUpTime;

    private bool player1PowerUpEnabled;
    private bool player2PowerUpEnabled;

    private void Awake() {
        instance = this;
    }

    private void Update() {
        if (player1PowerUpEnabled && (Time.time >= player1EndPowerUpTime)) {
            EnablePowerUp(false, 1);
        }

        if (player2PowerUpEnabled && (Time.time >= player2EndPowerUpTime)) {
            EnablePowerUp(false, 2);
        }
    }

    //ACTIVATE MASK
    public void ActivateMask(int player, ManaBall manaBall) {
        switch (player) {
            case 1:
                if(player1ManaBall) {
                    Destroy(player1ManaBall.gameObject);
                }
                player1ManaBall = manaBall;
                EnablePowerUp(true, 1);
                break;
            case 2:
                if (player2ManaBall) {
                    Destroy(player2ManaBall.gameObject);
                }
                player2ManaBall = manaBall;
                EnablePowerUp(true, 2);
                break;
        }
    }

    //ENABLE POWER UP
    void EnablePowerUp(bool value, int player) {
        if (player == 1) {
            player1PowerUpEnabled = value;
            if (value) {
                player1EndPowerUpTime = Time.time + powerUpDuration;
                player1Revelador.gameObject.SetActive(true);
                player1Revelador.target = GameManager.instance.player2Character.transform;
            }
            else {
                player1Revelador.target = null;
                player1Revelador.gameObject.SetActive(false);
                Destroy(player1ManaBall.gameObject);
            }

        }
        else if (player == 2) {
            player2PowerUpEnabled = value;
            if (value) {
                player2EndPowerUpTime = Time.time + powerUpDuration;
                player2Revelador.gameObject.SetActive(true);
                player2Revelador.target = GameManager.instance.player1Character.transform;
            }
            else {
                player2Revelador.target = null;
                player2Revelador.gameObject.SetActive(false);
                Destroy(player2ManaBall.gameObject);
            }


        }
    }

    //GET PLAYER TRANSFORM
    public Transform GetPlayerTransform(int player) {
        switch (player) {
            case 1:
                return player1Position;
                break;
            case 2:
                return player2Position;
                break;
        }

        return null;
    }
}
