﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public enum EGameState { MainMenu, Playing, SpawnSelection, RoundOver, GameOver }

    public static GameManager instance;

    [HideInInspector]
    public EGameState gameState;

    public bool debugMode;

    public PlayerController player1Character;
    public PlayerController player2Character;

    public Color player1Color;
    public Color player2Color;

    private float gameOverWaitTime = 1.5f;
    private float gameOverTimeCounter;

    private SpawnPoint player1SpawnPoint;
    private SpawnPoint player2SpawnPoint;

    private SpawnManager spawnerManager;
    private ManaSpawner manaSpawner;


    private void Awake() {
        instance = this;
        spawnerManager = GameObject.FindObjectOfType<SpawnManager>();
        manaSpawner = GameObject.FindObjectOfType<ManaSpawner>();
    }

    // Use this for initialization
    void Start() {
        ChangeGameState(EGameState.MainMenu);
    }


    //UPDATE
    private void Update() {
        if (gameState == EGameState.SpawnSelection) {
            CheckSpawnPointSelection();
        } else if(gameState == EGameState.GameOver) {
            CheckGameOver();
        }
    }

    //CHANGE GAME STATE
    public void ChangeGameState(EGameState newGameState) {
        gameState = newGameState;

        switch (gameState) {
            case EGameState.SpawnSelection:
                spawnerManager.SetupSpawnSelection();
                player1SpawnPoint = null;
                player2SpawnPoint = null;
                manaSpawner.SetActive(false);
                break;
            case EGameState.Playing:
                StartGame();
                manaSpawner.SetActive(true);
                break;
            case EGameState.RoundOver:
                RoundOver();
                break;
            case EGameState.GameOver:
                break;
        }
    }

    //CHECK SPAWN POINT SELECTION
    void CheckSpawnPointSelection() {
        if (player1SpawnPoint && player2SpawnPoint) {
            ChangeGameState(EGameState.Playing);
        }
        else {
            if (!player1SpawnPoint) {
                if (Input.GetAxis("B1") > 0) {
                    player1SpawnPoint = spawnerManager.GetSpawnPoint(1, SpawnPoint.ESpanwPointButtonID.B);
                    spawnerManager.HideSpawnPoints(1);
                }
                else if (Input.GetAxis("X1") > 0) {
                    player1SpawnPoint = spawnerManager.GetSpawnPoint(1, SpawnPoint.ESpanwPointButtonID.X);
                    spawnerManager.HideSpawnPoints(1);
                }
                else if (Input.GetAxis("Y1") > 0) {
                    player1SpawnPoint = spawnerManager.GetSpawnPoint(1, SpawnPoint.ESpanwPointButtonID.Y);
                    spawnerManager.HideSpawnPoints(1);
                }
            }

            if (!player2SpawnPoint) {
                if (Input.GetAxis("B2") > 0) {
                    player2SpawnPoint = spawnerManager.GetSpawnPoint(2, SpawnPoint.ESpanwPointButtonID.B);
                    spawnerManager.HideSpawnPoints(2);
                }
                else if (Input.GetAxis("X2") > 0) {
                    player2SpawnPoint = spawnerManager.GetSpawnPoint(2, SpawnPoint.ESpanwPointButtonID.X);
                    spawnerManager.HideSpawnPoints(2);
                }
                else if (Input.GetAxis("Y2") > 0) {
                    player2SpawnPoint = spawnerManager.GetSpawnPoint(2, SpawnPoint.ESpanwPointButtonID.Y);
                    spawnerManager.HideSpawnPoints(2);
                }
            }
        }
    }

    //START GAME
    void StartGame() {
        if (!debugMode) {
            player1Character.SetIsInvisible(true);
            player2Character.SetIsInvisible(true);
        }

        spawnerManager.HideSpawnPoints(0);

        player1Character.gameObject.transform.position = player1SpawnPoint.transform.position;
        player2Character.gameObject.transform.position = player2SpawnPoint.transform.position;

        ScoreManager.instance.player1ScoreTotem.PlaySpawnEffect(player1SpawnPoint.transform);
        ScoreManager.instance.player2ScoreTotem.PlaySpawnEffect(player2SpawnPoint.transform);

        player1Character.SetIsEnabled(true);
        player2Character.SetIsEnabled(true);
    }

    //ROUND OVER
    void RoundOver() {
        player1Character.SetIsEnabled(false);
        player2Character.SetIsEnabled(false);
        manaSpawner.SetActive(false);
        ChangeGameState(EGameState.SpawnSelection);
    }

    //CHECK GAME OVER
    void CheckGameOver() {
        if(gameOverTimeCounter >= gameOverWaitTime) {
            UIController.instance.GameOverMenu();
        } else {
            gameOverTimeCounter += Time.deltaTime;
        }
    }


    //PLAYER DEAD
    public void PlayerDead(int player) {
        switch (player) {
            case 1:
                ScoreManager.instance.AddScore(2);
                break;
            case 2:
                ScoreManager.instance.AddScore(1);
                break;
        }

        //CONTROLAR SI ALGUIEN GANO
        if (ScoreManager.instance.CheckWinner() != 0) {
            ChangeGameState(EGameState.GameOver);
        }
        else {
            ChangeGameState(EGameState.RoundOver);
        }

    }


}
