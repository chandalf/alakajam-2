﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vessel : MonoBehaviour {

    public Vector3 centerOfMass;
    Rigidbody rigidbody;
    AudioSource AS;

	// Use this for initialization
	void Awake () {
        rigidbody = GetComponent<Rigidbody>();
        AS = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Start () {
        rigidbody.centerOfMass = centerOfMass;
	}

    private void OnCollisionEnter(Collision collision) {
        if (collision.collider.GetComponent<PlayerController>()) {
            AS.pitch = Random.Range(0.75f, 1f);
            AS.Play();
        }    
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position + centerOfMass, 0.2f);
    }
}
